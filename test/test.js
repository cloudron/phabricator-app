#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const repodir = '/tmp/testrepo';
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    let email, adminEmail, token, fileNumber;

    const LOCATION = 'test';
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;

    let browser, app;

    before(function () {
        execSync('chmod o-rw,g-rw id_rsa', { stdio: 'inherit' });
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        fs.rmSync(repodir, { recursive: true, force: true });
        browser.quit();
    });

    async function login(u, p) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.xpath('//input[@name="username" and @type="text"]')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@name="username" and @type="text"]')).sendKeys(u);
        await browser.findElement(By.xpath('//input[@name="password" and @type="password"]')).sendKeys(p);
        await browser.findElement(By.xpath('//button[contains(text(), "Log In")]')).click();
        await browser.wait(until.elementLocated(By.xpath('//span[contains(text(), "Repositories") or contains(text(), "Differential")]')), TIMEOUT);
    }

    async function loginLdap(u, p, register) {
        await browser.get('https://' + app.fqdn);
        await browser.wait(until.elementLocated(By.xpath('//input[@name="ldap_username"]')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@name="ldap_username"]')).sendKeys(u);
        await browser.findElement(By.xpath('//input[@name="ldap_password"]')).sendKeys(p);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        if (!register) return;
        await browser.wait(until.elementLocated(By.xpath('//button[text()="Register Account"]')), TIMEOUT);
        await browser.findElement(By.xpath('//button[text()="Register Account"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//span[contains(text(), "Repositories") or contains(text(), "Differential")]')), TIMEOUT);
    }

    async function checkCloneUrl() {
        const cloneUrl = 'ssh://git@' + app.fqdn + ':29418/diffusion/1/testrepo.git';
        await browser.get('https://' + app.fqdn + '/diffusion/1/manage/uris/');
        await browser.wait(until.elementLocated(By.xpath('//a[contains(text(), "' + cloneUrl + '")]')), TIMEOUT);
    }

    async function checkCloning(checkNewFile) {
        fs.rmSync(repodir, { recursive: true, force: true });
        const env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync('git clone ssh://git@' + app.fqdn + ':29418/diffusion/1/testrepo.git ' + repodir, { env: env, stdio: 'inherit' });
        if (checkNewFile) expect(fs.existsSync(repodir + '/newfile')).to.be(true);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    }

    async function setupAdminAccount(username /*, password */) {
        await browser.get('https://' + app.fqdn + '/auth/register/');
        await browser.wait(until.elementLocated(By.xpath('//span["Welcome to Phabricator"]')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@type="text" and @name="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@type="text" and @name="realName"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@type="text" and @name="email"]')).sendKeys(adminEmail);
        await browser.findElement(By.xpath('//button[contains(text(), "Create Admin Account")]')).click();
        await browser.wait(until.elementLocated(By.xpath('//span[contains(text(), "Recent Activity")]')), TIMEOUT);
    }

    async function setupAccount(username, password) {
        await browser.manage().deleteAllCookies();
        browser.get('https://' + app.fqdn + '/auth/register/');
        await browser.wait(until.elementLocated(By.xpath('//span["Create a New Account"]')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@type="text" and @name="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@type="text" and @name="realName"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@type="password" and @name="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//input[@type="password" and @name="confirm"]')).sendKeys(password);
        await browser.findElement(By.xpath('//input[@type="text" and @name="email"]')).sendKeys(email);
        await browser.findElement(By.xpath('//button[contains(text(), "Register Account")]')).click();
        await browser.wait(until.elementLocated(By.xpath('//span[contains(text(), "Recent Activity")]')), TIMEOUT);
    }

    async function setCreateRepoPolicy() {
        await browser.get('https://' + app.fqdn + '/applications/edit/PhabricatorDiffusionApplication/');
        await browser.wait(until.elementLocated(By.xpath('//label[contains(text(), "Can Create Repositories")]')), TIMEOUT);
        console.log('Manually change Can Create Repositories And Default Edit Policy to "All Users" and click save');
        await browser.sleep(120000);
    }

    async function canGetMainPage() {
        const response = await superagent.get('https://' + app.fqdn).ok(() => true);
        expect(response.status).to.eql(200); // will redirect to register page
        expect(response.redirects[0]).to.be('https://' + app.fqdn + '/auth/register/');
    }

    async function createRepo() {
        await browser.get('https://' + app.fqdn + '/diffusion/edit/');
        await browser.sleep(TIMEOUT);
        await browser.findElement(By.xpath('//span[contains(text(), "Create a new Git repository.")]')).click();
        await browser.sleep(TIMEOUT); // wait for ?vcs=git to load
        await browser.findElement(By.xpath('//input[@name="name" and @type="text"]')).sendKeys('testrepo');
        await browser.findElement(By.xpath('//button[contains(text(), "Create Repository")]')).click(); // "." means innerText apparently
        await browser.wait(async function () {
            const url = await browser.getCurrentUrl();
            return url === 'https://' + app.fqdn + '/diffusion/1/manage/';
        }, TIMEOUT * 4);
    }

    async function pushToRepo() {
        await browser.get('https://' + app.fqdn + '/diffusion/1/edit/activate/');
        await browser.findElement(By.xpath('//button[contains(text(), "Activate Repository")]')).click();
        // https://secure.phabricator.com/book/phabricator/article/diffusion_updates/
        await browser.get('https://' + app.fqdn + '/diffusion/1/edit/update/');
        await browser.findElement(By.xpath('//button[contains(text(), "Schedule Update")]')).click();
        console.log('Sleeping for ' + (TIMEOUT * 3) + ' for the diffusion to catch up');
        await browser.sleep(TIMEOUT * 3); // it takes sometime for the repo to get created
        const env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync('git push -f ssh://git@' + app.fqdn + ':29418/diffusion/1/testrepo.git master', { env: env, stdio: 'inherit' }); // push this repo
    }

    async function uploadFile() {
        await browser.get('https://' + app.fqdn + '/file/upload');
        execSync('truncate -s 2M /tmp/bigfile.txt'); // if it's < 1M it is stored in sql db. so create something big enough
        await browser.findElement(By.xpath('//input[@type="file" and @name="file"]')).sendKeys('/tmp/bigfile.txt');
        await browser.findElement(By.xpath('//button[contains(text(), "Upload")]')).click();
        await browser.wait(async function () {
            const url = await browser.getCurrentUrl();
            if (!url.startsWith('https://' + app.fqdn + '/F')) return false;
            fileNumber = url.substr(('https://' + app.fqdn + '/').length);
            console.log('file number is ', fileNumber);
            return true;
        }, 40000);
    }

    async function createManiphestTask() {
        await browser.get('https://' + app.fqdn + '/maniphest/task/edit/');
        await browser.findElement(By.xpath('//input[@name="title" and @type="text"]')).sendKeys('this is a task');
        const button = browser.findElement(By.xpath('//button[contains(text(), "Create New Task")]'));
        await browser.executeScript('arguments[0].scrollIntoView(false)', button);
        await browser.findElement(By.xpath('//button[contains(text(), "Create New Task")]')).click();
        await browser.wait(async function () {
            const url = await browser.getCurrentUrl();
            return url === 'https://' + app.fqdn + '/T1';
        }, 4000);
    }

    async function checkSetupItems() {
        await browser.get('https://' + app.fqdn);
        await browser.findElement(By.xpath('//span[@class="phabricator-main-menu-setup-count" and text()="1"]'));
    }

    async function checkNoSetupItems() {
        await browser.get('https://' + app.fqdn);
        browser.findElement(By.xpath('//span[@class="phabricator-main-menu-setup-count"]')).then(function () {
            throw new Error('Has some setup errors');
        }).catch(function () { // success
        });
    }

    async function addPublicKey() {
        const publicKey = fs.readFileSync(__dirname + '/id_rsa.pub', 'utf8');

        await browser.get('https://' + app.fqdn + '/settings/panel/ssh/');
        await browser.findElement(By.xpath('//div[contains(text(), "SSH Key Actions")]')).click();
        await browser.sleep(2000); // let the menu appear
        await browser.findElement(By.xpath('//a[contains(text(), "Upload Public Key")]')).click();
        await browser.sleep(2000); // wait for dialog to appear
        await browser.findElement(By.xpath('//input[@name="name" and @type="text"]')).sendKeys('testkey');
        await browser.findElement(By.xpath('//textarea[@name="key"]')).sendKeys(publicKey);
        await browser.findElement(By.xpath('//button[contains(text(), "Upload Public Key")]')).click();
        await browser.sleep(TIMEOUT); // wait for request to succeed before nex test
    }

    async function pushFile() {
        const env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync('touch newfile && git add newfile && git commit -a -mx && git push ssh://git@' + app.fqdn + ':29418/diffusion/1/testrepo.git master',
            { env: env, cwd: repodir, stdio: 'inherit' });
        fs.rmSync(repodir, { recursive: true, force: true });
    }

    async function checkManiphestTask() {
        await browser.get('https://' + app.fqdn + '/T1');
        await browser.findElement(By.xpath('//span[contains(text(), "this is a task")]'));
    }

    async function checkFile() {
        await browser.get('https://' + app.fqdn + '/' + fileNumber);
        await browser.findElement(By.xpath('//span[contains(text(), "bigfile.txt")]'));
    }

    async function empower(u) {
        execSync(`cloudron exec --app ${app.fqdn} -- /app/code/phabricator/bin/user empower --user ${u}`);
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', async function () {
        const inspect = JSON.parse(execSync('cloudron inspect'));

        let response = await superagent.post('https://' + inspect.apiEndpoint + '/api/v1/cloudron/login').send({ username, password });
        token = response.body.accessToken;

        response = await superagent.get('https://' + inspect.apiEndpoint + '/api/v1/profile')
            .query({ access_token: token });

        email = response.body.email;
        adminEmail = 'admin+' + response.body.email;
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('can get app information', getAppInfo);
    it('can login with LDAP', loginLdap.bind(null, username, password, true /* register */));
    it('can become admin', empower.bind(null, username));
    it('can set create repo policy', setCreateRepoPolicy);
    it('must have no setup items to complete', checkNoSetupItems);
    it('can create a maniphest task', createManiphestTask);
    it('can add public key', addPublicKey);
    it('can create repo', createRepo);
    it('displays correct clone url', checkCloneUrl);
    it('can push to repo', pushToRepo);
    it('can clone the url', checkCloning.bind(null, false /* newfile */));
    it('can add and push a file', pushFile);
    it('can upload a file', uploadFile);

    it('can restart app', function (done) {
        execSync('cloudron restart');
        done();
    });

    it('can clone the url', checkCloning.bind(null, true /* newfile */));

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        execSync('cloudron install --no-sso --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can clone the url', checkCloning.bind(null, true /* newfile */));

    it('has the task', checkManiphestTask);
    it('has the file', checkFile);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
        browser.sleep(TIMEOUT); // sometimes phabricator lies about being ready
    });

    it('can login', loginLdap.bind(null, username, password, false /* register */));
    it('displays correct clone url', checkCloneUrl);
    it('can clone the url', checkCloning.bind(null, true /* check newfile */));

    it('has the task', checkManiphestTask);
    it('has the file', checkFile);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app (no sso)', function () {
        execSync('cloudron install --no-sso --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('can get app information (no sso)', getAppInfo);
    it('can get the main page (no sso)', canGetMainPage);
    it('can setup admin account (no sso)', setupAdminAccount.bind(null, 'admin', password));
    it('must have only one setup item to complete (no sso)', checkSetupItems);
    it('uninstall app (no sso)', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --appstore-id org.phabricator.cloudronapp2 --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('can get app information', getAppInfo);
    it('can login with LDAP', loginLdap.bind(null, username, password, true /* register */));
    it('can become admin', empower.bind(null, username));
    it('can set create repo policy', setCreateRepoPolicy);
    xit('can register new account', setupAccount.bind(null, username, password));
    xit('can login using default credentials', function (done) {
        login.bind(null, username, password)(done);
    });
    it('can create a maniphest task', createManiphestTask);
    it('can add public key', addPublicKey);
    it('can create repo', createRepo);
    it('displays correct clone url', checkCloneUrl);
    it('can push to repo', pushToRepo);
    it('can clone the url', checkCloning.bind(null, false /* newfile */));
    it('can add and push a file', pushFile);
    it('can upload a file', uploadFile);

    it('can update', function () {
        execSync('cloudron update --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can clone the url', checkCloning.bind(null, true /* newfile */));
    it('has the task', checkManiphestTask);
    it('has the file', checkFile);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
