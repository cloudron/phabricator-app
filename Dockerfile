FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

# (stable) Promote 2022 Week 21
# https://github.com/phacility/arcanist/commits/stable
ARG ARCANIST_VERSION=b6babd9a07f428c68863cb38f2fa87114e9ca2eb
# (stable) Promote 2022 Week 21
# https://github.com/phacility/phabricator/commits/stable
ARG PHABRICATOR_VERSION=d075f3e93c023b3f2cdd13d63dddf4707400058f

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# without this apt-add-repository fails
ENV LANG=C.UTF-8

RUN apt-get update && \
    apt-get install -y python3-pygments subversion mercurial openssh-server git && \
    rm -r /var/cache/apt /var/lib/apt/lists

# by default, git account is created as inactive which prevents login via openssh
# https://github.com/gitlabhq/gitlabhq/issues/5304
RUN adduser --disabled-login --gecos 'Git' git && passwd -d git
RUN adduser --disabled-login --gecos 'Phabricator Deamon' phd

# https://secure.phabricator.com/book/phabricator/article/diffusion_hosting
RUN echo "git ALL=(phd) SETENV: NOPASSWD: /usr/bin/git-upload-pack, /usr/bin/git-receive-pack, /usr/bin/hg, /usr/bin/svnserve" >> /etc/sudoers

RUN mkdir arcanist && \
    cd arcanist && \
    curl -L https://github.com/phacility/arcanist/archive/${ARCANIST_VERSION}.tar.gz | tar -xzf - --strip-components 1

RUN mkdir phabricator && \
    cd phabricator && \
    curl -L https://github.com/phacility/phabricator/archive/${PHABRICATOR_VERSION}.tar.gz | tar -xzf - --strip-components 1
ADD local.json.template /app/pkg/
RUN ln -s /app/data/local.json /app/code/phabricator/conf/local/local.json 

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache2-phabricator.conf /etc/apache2/sites-available/phabricator.conf
RUN ln -sf /etc/apache2/sites-available/phabricator.conf /etc/apache2/sites-enabled/phabricator.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod rewrite
RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 512M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 512M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit -1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/phabricator/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100 && \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.validate_timestamps 0 && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP always_populate_raw_post_data -1 && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP mysqli.allow_local_infile 0

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

# configure supervisor
RUN sed -e 's,^logfile=.*$,logfile=/run/phabricator/supervisord.log,' -i /etc/supervisor/supervisord.conf
ADD supervisor/ /etc/supervisor/conf.d/

ADD sshd_config /app/code/sshd_config
ADD phabricator-ssh-hook.sh /app/code/phabricator-ssh-hook.sh
ADD preamble.php /app/code/phabricator/support/preamble.php

ADD start.sh /app/pkg/start.sh

CMD [ "/app/pkg/start.sh" ]

