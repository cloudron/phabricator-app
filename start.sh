#!/bin/bash

set -eu -o pipefail

mkdir -p /run/phabricator/phd /run/sshd

if [[ -z "${SSH_PORT:-}" ]]; then
    echo "==> SSH Disabled"
    SSH_PORT=29418
fi

[[ ! -f /app/data/local.json ]] && cp /app/pkg/local.json.template /app/data/local.json

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

# patch up configuration file
jq ".[\"phabricator.base-uri\"] = \"${CLOUDRON_APP_ORIGIN}\"" /app/data/local.json | sponge /app/data/local.json
jq ".[\"storage.default-namespace\"] = \"${CLOUDRON_MYSQL_DATABASE_PREFIX%_}\"" /app/data/local.json | sponge /app/data/local.json
jq ".[\"mysql.host\"] = \"${CLOUDRON_MYSQL_HOST}\"" /app/data/local.json | sponge /app/data/local.json
jq ".[\"mysql.port\"] = \"${CLOUDRON_MYSQL_PORT}\"" /app/data/local.json | sponge /app/data/local.json
jq ".[\"mysql.user\"] = \"${CLOUDRON_MYSQL_USERNAME}\"" /app/data/local.json | sponge /app/data/local.json
jq ".[\"mysql.pass\"] = \"${CLOUDRON_MYSQL_PASSWORD}\"" /app/data/local.json | sponge /app/data/local.json
jq ".[\"diffusion.ssh-port\"] = ${SSH_PORT}" /app/data/local.json | sponge /app/data/local.json
jq ".[\"metamta.default-address\"] = \"${CLOUDRON_MAIL_FROM}\"" /app/data/local.json | sponge /app/data/local.json
jq ".[\"cluster.mailers\"][0].options.host = \"${CLOUDRON_MAIL_SMTP_SERVER}\"" /app/data/local.json | sponge /app/data/local.json
jq ".[\"cluster.mailers\"][0].options.port = ${CLOUDRON_MAIL_SMTP_PORT}" /app/data/local.json | sponge /app/data/local.json
jq ".[\"cluster.mailers\"][0].options.user = \"${CLOUDRON_MAIL_SMTP_USERNAME}\"" /app/data/local.json | sponge /app/data/local.json
jq ".[\"cluster.mailers\"][0].options.password = \"${CLOUDRON_MAIL_SMTP_PASSWORD}\"" /app/data/local.json | sponge /app/data/local.json

# https://secure.phabricator.com/book/phabricator/article/configuring_file_storage/
mkdir -p /app/data/filestorage /app/data/repo
chown -R phd:phd /app/data/repo /run/phabricator/phd
chown -R www-data:www-data /app/data/filestorage /app/data/local.json

echo "==> Upgrading database"
/app/code/phabricator/bin/storage upgrade --force

if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
    readonly ldap_config="{\"ldap:port\":\"${CLOUDRON_LDAP_PORT}\",\"ldap:version\":\"3\",\"ldap:host\":\"${CLOUDRON_LDAP_SERVER}\",\"ldap:dn\":\"${CLOUDRON_LDAP_USERS_BASE_DN}\",\"ldap:search-attribute\":\"(|(username=\${login})(mail=\${login}))\",\"ldap:anoynmous-username\":\"${CLOUDRON_LDAP_BIND_DN}\",\"ldap:anonymous-password\":\"${CLOUDRON_LDAP_BIND_PASSWORD}\",\"ldap:username-attribute\":\"username\",\"ldap:realname-attributes\":[\"displayname\"],\"ldap:activedirectory-domain\":\"\"}"

    mysql -u"${CLOUDRON_MYSQL_USERNAME}" -p"${CLOUDRON_MYSQL_PASSWORD}" -h "${CLOUDRON_MYSQL_HOST}" -P "${CLOUDRON_MYSQL_PORT}" --database="${CLOUDRON_MYSQL_DATABASE_PREFIX}auth" \
        -e "DELETE FROM \`auth_providerconfig\` WHERE providerClass='PhabricatorLDAPAuthProvider'"

    if mysql -u"${CLOUDRON_MYSQL_USERNAME}" -p"${CLOUDRON_MYSQL_PASSWORD}" -h "${CLOUDRON_MYSQL_HOST}" -P "${CLOUDRON_MYSQL_PORT}" --database="${CLOUDRON_MYSQL_DATABASE_PREFIX}auth" \
        -e "INSERT INTO \`auth_providerconfig\` VALUES (1,_binary 'PHID-AUTH-5fwakdib5lt3zqwbtplf','PhabricatorLDAPAuthProvider','ldap','self',1,1,1,1,0,1,'${ldap_config}',1595613349,1595613390,0);"; then
        echo "==> LDAP configuration auto setup successfully"
    else
        echo "==> Failed to setup LDAP authentication"
    fi

    # lock the config, otherwise it appears as a setup issue. maybe do this only for first run?
    jq ".[\"auth.lock-config\"] = true" /app/data/local.json | sponge /app/data/local.json
fi

# TODO: roll this as a supervisor script (http://blog.spang.cc/posts/running_phd_under_supervisor/)
echo "==> Starting daemons"
/usr/local/bin/gosu phd:phd /app/code/phabricator/bin/phd restart

echo "==> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Phabricator

