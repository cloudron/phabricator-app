<sso>
To give admin rights to a user, please follow the [docs](https://cloudron.io/documentation/apps/phabricator/#empower).
</sso>

<nosso>
On first visit, you will be asked to set up the admin password. You should immediately
proceed to setup an Auth provider like the `Username/Password` provider. Then, create a user
and give that user admin rights. This is important because Phabricator has a quirk that the initial
admin in Phabricator cannot "re-login".
</nosso>

